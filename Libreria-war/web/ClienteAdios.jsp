<%@page import="entidad.*,stateless.*, java.math.BigDecimal, javax.naming.*,java.util.*" %>

<%!
    private LibroBeanRemote librocat = null;
    String s0, s1, s2, s3;
    Collection list;

    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando el Catalogo" + librocat);
        } catch (Exception ex) {
            System.out.println("Error" + ex.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>

<%
    try {
        s0 = request.getParameter("t1");

        if (s0 != null) {
            list = librocat.buscaLibro(Integer.parseInt(s0));
            for (Iterator iter = list.iterator(); iter.hasNext();) {
                Libro elemento = (Libro) iter.next();
                s1 = elemento.getTitulo();
                s2 = elemento.getAutor();
                s3 = elemento.getPrecio().toString();
            }
        }

%>

<jsp:forward page="formaB.jsp">
    <jsp:param name="id" value="<%=s0%>"/>
    <jsp:param name="t1" value="<%=s1%>"/>
    <jsp:param name="aut" value="<%=s2%>"/>
    <jsp:param name="precio" value="<%=s3%>"/>
</jsp:forward>

<%

    } catch (Exception e) {
        e.printStackTrace();
    }

%>