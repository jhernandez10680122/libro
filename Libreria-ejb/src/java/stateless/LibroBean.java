/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Daftzero
 */
@Stateless
public class LibroBean implements LibroBeanRemote {

    @PersistenceContext(unitName = "Libreria-ejbPU")
    EntityManager em;
    protected Libro libro;
    protected Collection<Libro> listaLibros;

    @Override
    public void addLibro(String Titulo, String autor, BigDecimal precio) {
        if (libro == null) {
            libro = new Libro(Titulo, autor, precio);
            em.persist(libro);
            libro = null;
        }
    }

    @Override
    public void actualizaLibro(int id, String Titulo, String autor, BigDecimal precio) {
       Libro libroAct = em.find(Libro.class, id);
        if (libroAct != null) {
            libroAct.setTitulo(Titulo);
            libroAct.setAutor(autor);
            libroAct.setPrecio(precio);
            em.merge(libroAct);
        }
    }

    @Override
    public Collection<Libro> getAllLibros() {
        TypedQuery<Libro> query = em.createNamedQuery("Libro.findAll", Libro.class);
        Collection<Libro> listaLibros = query.getResultList();
        return listaLibros;
    }

    @Override
    public void removeLibro(int id) {
        Libro emp = findLibro(id);
        if (emp != null) {
            em.remove(emp);
        }
    }

    @Override
    public Libro findLibro(int id) {
        return em.find(Libro.class, id);
    }

    @Override
    public Collection<Libro> buscaLibro(int id) {
        TypedQuery<Libro> query = em.createNamedQuery("Libro.findById", Libro.class).setParameter("id", id);
        Collection<Libro> listaLibros = query.getResultList();
        return listaLibros;
    }

}
