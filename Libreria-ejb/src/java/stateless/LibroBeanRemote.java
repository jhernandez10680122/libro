/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;


import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Remote;

/**
 *
 * @author Daftzero
 */
@Remote
//bin de sesion para anadir un nuevo registro 
public interface LibroBeanRemote {
    
    public void addLibro(String titulo, String autor, BigDecimal precio);
    public Collection <Libro> getAllLibros();
    public Collection <Libro> buscaLibro(int id);
    public void actualizaLibro(int id, String Titulo, String autor, BigDecimal precio);
    public Libro findLibro(int id);

    public void removeLibro(int id);
    
}
